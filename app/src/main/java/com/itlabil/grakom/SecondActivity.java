package com.itlabil.grakom;

import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class SecondActivity extends AppCompatActivity {

    private SoundPool soundPool;

    private AudioManager audioManager;

    // Max sound stream
    private static final int MAX_STREAMS = 5;

    // Stream type.
    private static final int streamType = AudioManager.STREAM_MUSIC;

    private boolean loaded;

    private int indo,inggris;

    private float volume;

    ImageView selectedImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        selectedImage = (ImageView) findViewById(R.id.selectedImage); // init a ImageView
        Intent intent = getIntent(); // get Intent which we set from Previous Activity
        selectedImage.setImageResource(intent.getIntExtra("image", 0)); // get image from Intent and set it in ImageView

        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        float currentVolumeIndex = (float) audioManager.getStreamVolume(streamType);
        float maxVolumeIndex  = (float) audioManager.getStreamMaxVolume(streamType);

        // Volumn (0 --> 1)
        this.volume = currentVolumeIndex / maxVolumeIndex;
        this.setVolumeControlStream(streamType);

        // Untuk Android SDK >= 21
        if (Build.VERSION.SDK_INT >= 21 ) {

            AudioAttributes audioAttrib = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            SoundPool.Builder builder= new SoundPool.Builder();
            builder.setAudioAttributes(audioAttrib).setMaxStreams(MAX_STREAMS);

            this.soundPool = builder.build();
        }
        // untuk Android SDK < 21
        else {
            this.soundPool = new SoundPool(MAX_STREAMS, AudioManager.STREAM_MUSIC, 0);
        }

        // Ketika Sound Pool Selesai di Load.
        this.soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                loaded = true;
            }
        });

        int soundIndo = intent.getIntExtra("soundIndo", 0);
        int soundInggris = intent.getIntExtra("soundInggris", 0);

        // Library Sound indo
        this.indo = this.soundPool.load(this, soundIndo,1);
        this.inggris = this.soundPool.load(this, soundInggris,1);
    }

    public void indo(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int indo = this.soundPool.play(this.indo,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

    public void inggris(View view) {
        if(loaded)  {
            float leftVolumn = volume;
            float rightVolumn = volume;
            int inggris = this.soundPool.play(this.inggris,leftVolumn, rightVolumn, 1, 0, 1f);
        }
    }

}
