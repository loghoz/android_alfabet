package com.itlabil.grakom;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    GridView simpleGrid;

    int logos[] = {R.drawable.ic_besar_a,R.drawable.ic_besar_b,R.drawable.ic_besar_c,R.drawable.ic_besar_d,R.drawable.ic_besar_e
            ,R.drawable.ic_besar_f,R.drawable.ic_besar_g,R.drawable.ic_besar_h,R.drawable.ic_besar_i,R.drawable.ic_besar_j
            ,R.drawable.ic_besar_k,R.drawable.ic_besar_l,R.drawable.ic_besar_m,R.drawable.ic_besar_n,R.drawable.ic_besar_o
            ,R.drawable.ic_besar_p,R.drawable.ic_besar_q,R.drawable.ic_besar_r,R.drawable.ic_besar_s,R.drawable.ic_besar_t
            ,R.drawable.ic_besar_u,R.drawable.ic_besar_v,R.drawable.ic_besar_w,R.drawable.ic_besar_x,R.drawable.ic_besar_y
            ,R.drawable.ic_besar_z};

    int soundIndo[] = {R.raw.indo_a,R.raw.indo_b,R.raw.indo_c,R.raw.indo_d,R.raw.indo_e
            ,R.raw.indo_f,R.raw.indo_g,R.raw.indo_h,R.raw.indo_i,R.raw.indo_j
            ,R.raw.indo_k,R.raw.indo_l,R.raw.indo_m,R.raw.indo_n,R.raw.indo_o
            ,R.raw.indo_p,R.raw.indo_q,R.raw.indo_r,R.raw.indo_s,R.raw.indo_t
            ,R.raw.indo_u,R.raw.indo_f,R.raw.indo_w,R.raw.indo_x,R.raw.indo_y
            ,R.raw.indo_z};

    int soundInggris[] = {R.raw.inggris_a,R.raw.inggris_b,R.raw.inggris_c,R.raw.inggris_d,R.raw.inggris_e
            ,R.raw.inggris_f,R.raw.inggris_g,R.raw.inggris_h,R.raw.inggris_i,R.raw.inggris_j
            ,R.raw.inggris_k,R.raw.inggris_l,R.raw.inggris_m,R.raw.inggris_n,R.raw.inggris_o
            ,R.raw.inggris_p,R.raw.inggris_q,R.raw.inggris_r,R.raw.inggris_s,R.raw.inggris_t
            ,R.raw.inggris_u,R.raw.inggris_f,R.raw.inggris_w,R.raw.inggris_x,R.raw.inggris_y
            ,R.raw.inggris_z};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        simpleGrid = (GridView) findViewById(R.id.simpleGridView); // init GridView
        // Create an object of CustomAdapter and set Adapter to GirdView
        CustomAdapter customAdapter = new CustomAdapter(getApplicationContext(), logos);
        simpleGrid.setAdapter(customAdapter);
        // implement setOnItemClickListener event on GridView

        simpleGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // set an Intent to Another Activity
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("image", logos[position]); // put image data in Intent
                intent.putExtra("soundIndo", soundIndo[position]); // put sound Indo in Intent
                intent.putExtra("soundInggris", soundInggris[position]); // put sound Inggris in Intent
                startActivity(intent); // start Intent
            }
        });
    }
}
